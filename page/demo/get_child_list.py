#!/usr/bin/python3
# -*- coding: UTF-8 -*- 
# 设置utf-8  显示中文
"""
@Create Time: 2020-11-20 15:53
@Author: guozg
@File：get_child_list.py
"""
from base.api_base import ApiBase
# page 对应的yml所在的目录,不包括data目录.即data下面的目录.
page_yml_dir = 'demo'

class GetChild(ApiBase):

    def get_child(self, params: dict = None):
        params = params
        return self.steps(params=params,page_yml_dir=page_yml_dir)

    def get_classes(self, params: dict = None):
        return self.steps(params=params,page_yml_dir=page_yml_dir)

    def get_stayover(self, params: dict = None):
        return self.steps(params=params,page_yml_dir=page_yml_dir)

    def get_daiban(self, params: dict = None):
        return self.steps(params=params,page_yml_dir=page_yml_dir)

    def get_leavelist(self, params: dict = None):
        return self.steps(params=params,page_yml_dir=page_yml_dir)


if __name__ == '__main__':
    child = GetChild()
    child.get_child()
    child.get_classes()
