#!/usr/bin/python3
# -*- coding: UTF-8 -*- 
# 设置utf-8  显示中文
"""
@Author: guo
@File：get_app_cookie_header.py
"""
import requests


class GetAppCookieHeader:
    '''
    公司的app的cookie是以{"user":userid,"session":sesson} 这种方式来传递的。与web端有所不同。
    '''

    def __init__(self, response: requests.models.Response):
        '''
        实例化时，需要传入请求的响应结果，即Response \n
        :param response: request的请求结果Response
        '''
        self.__req = response
        self.__header = self.__get_header()

    def __get_header(self):
        header = None
        if self.__req.status_code == 200:
            header = {}
            userid = self.__req.json().get("userid")
            session = self.__req.json().get("session")
            header = {
                "user": str(userid),
                "session": session
            }

        return header

    def get_header(self) -> dict:
        '''
        获取session及userid，并进行拼接及返回，其他接口在进行请求时，需要用到。  \n
        :return: dict
        '''
        return self.__header
