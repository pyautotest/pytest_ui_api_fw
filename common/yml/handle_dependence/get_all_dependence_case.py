#!/usr/bin/python3
# -*- coding: UTF-8 -*- 
# 设置utf-8  显示中文
"""
@Author: guo
@File：get_all_dependence_case.py
"""
import sys
sys.path.append("../../")


from common.util.handle_jsonfile import HandleJsonFile
from config.get_conf_data import GetConfData

from common.yml.handle_api_yml_data import HandleYmlData
from common.yml.handle_dependence.check_isNone import CheckNone
from common.yml.handle_dependence.get_caseid import GetCaseId
from common.yml.handle_dependence.get_yml_func_data import GetFuncData




class GetAllDepCase:
    """"""
    def __init__(self):
        self.__conf = GetConfData()
        self.__hjson = HandleJsonFile()

        # ***************分割线***********************
        self.__caseId = GetCaseId()
        self.__funcData = GetFuncData()
        self.__checkNone = CheckNone()

    def get_all_case(self, ymldata: dict) -> list:
        """
        获取当前case所依赖的所有的case_id,并返回 \n
        最后返回的为list,但list的值为dict \n
        dict的值为: {'caseid':'xxxxx','ymldata':'xxxxx','testinit':'xxxxxxx'} \n
        其中 caseid的值为: case_id的值  \n
        ymldata的值为: case_id 所对应的yml文件处理后的数据  \n
        testinit的值为: 每个class下获取登录结果的方法名称,该名称与 该方法对应的yml文件里的 key是一致的. \n
        :param ymldata: 处理后的yml 数据.
        :return: list
        """
        caseid_dict = self.__caseId.get_caseid(ymldata)
        # 最终的数据
        final = []
        # case层与page层的分割标志
        case_join_page_flag = self.__conf.get_case_join_page()
        # 获取登录结果的方法名称
        login_func_name = self.__conf.get_login_func_name()


        # ****************** 保存结果 需要用到的一些key **********************
        # 保存的依赖caseid 所对应的key
        caseid_key = self.__conf.get_caseid()
        # 保存依赖的case 所对应的数据类型,是json还是yml 或者 sql.
        # 当依赖的case为 response 或 request时,均为yml类型.
        filetype_key = self.__conf.get_filetype()
        # 保存依赖case所对应的yml数据 所对应的key
        filedata_key = self.__conf.get_filedata()
        # 保存 初始化获取登录结果的case名称，所对应的key
        testinit_key = self.__conf.get_testinit()

        # **************** 需要用到的一些字符串变量 ***********************
        json_str = self.__conf.get_json_str()
        sql_str = self.__conf.get_sql_str()

        # ******************** 获取caseid的值 ****************************
        caseid = None
        # 当不为空时,直接获取caseid的值.
        if  bool(caseid_dict) :
            caseid = caseid_dict[caseid_key]
            '''
            此时要先判断caseid 是否为None.
            1:不为None时,表明此时依赖的类型为 request 或 response.
            2:为None,表示此时依赖的类型为 json 或 sql
            3:当为json时,直接读取指定的json文件中的内容(不在校验文件是否存在),然后再将filedata里的值,替换为读取json文件的内容.
            4:当为sql时,暂时未设计,后续再考虑.目前是直接将sql语句,赋值到filedata中
            '''
            if caseid ==None:
                # 获取filetype的值
                filetype = caseid_dict[filetype_key]
                # 当为json时,获取json文件的值.先不考虑sql的情况.
                if filetype == json_str:
                    filedata = caseid_dict[filedata_key]
                    filedata = self.__hjson.read_json_file_to_dict(filedata)
                    caseid_dict[filedata_key] = filedata

                # 为了保持统一,也为了方便后续的取值方便,加入testinit
                caseid_dict[testinit_key] = None

                final.append(caseid_dict)


        while caseid != None:
            """"""
            # 每组依赖的数据
            tmp_dict = {}

            caseid_list = caseid.split(case_join_page_flag)
            casestr = self.__checkNone.check_isNone(caseid_list[0])
            pagestr = self.__checkNone.check_isNone(caseid_list[1])
            # 当pagestr为None时,表示为登录(目前不考虑其他情况)
            if pagestr == None:
                case_yml_data = self.__funcData.get_yml_func_data(casestr)
                # 保存case_id值
                tmp_dict[caseid_key] = caseid
                # 保存依赖的接口的请求数据
                tmp_dict[filedata_key] = case_yml_data
                # 保存登录的方法名称
                tmp_dict[testinit_key] = login_func_name
                final.append(tmp_dict)
                # 重置caseid
                caseid = None

            else:
                # 获取case层的yml数据
                case_yml_data = self.__funcData.get_yml_func_data(casestr)
                # 获取page层的yml数据
                page_yml_data = self.__funcData.get_yml_func_data(pagestr)
                # 处理case层yml数据和page层yml数据,合二为一
                yml_data = HandleYmlData(page_yml_data, case_yml_data).get_handle_yml_data()
                # 保存case_id值
                tmp_dict[caseid_key] = caseid
                # 保存依赖的接口的请求数据
                tmp_dict[filedata_key] = yml_data
                # 保存登录的方法名称
                tmp_dict[testinit_key] = login_func_name
                final.append(tmp_dict)
                # 重置caseid
                caseid = self.__caseId.get_caseid(yml_data)
        return final


