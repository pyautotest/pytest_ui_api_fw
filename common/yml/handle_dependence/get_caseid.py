#!/usr/bin/python3
# -*- coding: UTF-8 -*- 
# 设置utf-8  显示中文
"""
@Author: guo
@File：get_caseid.py
"""
from common.yml.get_yml_keys import GetYmlKeys
from config.get_conf_data import GetConfData


class GetCaseId:
    """"""

    def __init__(self):
        self.__ymlkeys = GetYmlKeys()
        self.__conf = GetConfData()
        self.__jsonstr = self.__conf.get_json_str()
        self.__sqlstr = self.__conf.get_sql_str()
        self.__ymlstr = self.__conf.get_yml_str()
        self.__reqstr = self.__conf.get_request_str()
        self.__repstr = self.__conf.get_response_str()
        self.__caseid = self.__conf.get_caseid()
        self.__filetype = self.__conf.get_filetype()
        self.__filedata = self.__conf.get_filedata()

    def get_caseid(self, ymldata: dict) -> dict:
        """获取依赖的caseid. 当有数据依赖时,会返回依赖的caseid,filetype,filedata.没有时,返回None"""
        # 需要注意的是,传入的ymldata为处理后的yml数据
        ymldata = ymldata
        ymlkeys = self.__ymlkeys
        # 获取api_data数据
        apidata_key = ymlkeys.get_api_data_key()
        api_data = ymldata[apidata_key]
        # 获取是否有接口数据依赖 的值
        rely_key = ymlkeys.get_dependence_case_key()
        rely_case = api_data[rely_key]

        case_data_key = ymlkeys.get_dependence_case_data_key()
        caseid_key = ymlkeys.get_dependence_case_id_key()
        type_key = ymlkeys.get_dependence_type_key()
        jsonfilepath_key = ymlkeys.get_jsonfilepath_key()
        sqlstatement_key = ymlkeys.get_sqlstatement_key()

        value_data = {}
        # 当有接口数据依赖时,需要保存caseid,filetype,filedata,
        if rely_case == True:

            """"""
            # 获取依赖数据
            casedata = api_data[case_data_key]
            # 当存在依赖数据时
            if casedata != None:
                # 获取依赖的case_id
                caseid = casedata[caseid_key]
                filetype = casedata[type_key]
                # 判断是否json
                if filetype == self.__jsonstr:
                    filedata = casedata[jsonfilepath_key]
                # 判断是否为sql
                elif filetype == self.__sqlstr:
                    filedata = casedata[sqlstatement_key]
                # 当为request 或 response时,不需要对filedata进行赋值,因为在处理数据依赖时,会再次获取依赖的case的yml数据.
                else:
                    filedata = None

                value_data[self.__caseid] = caseid
                value_data[self.__filetype] = filetype
                value_data[self.__filedata] = filedata

        return value_data
