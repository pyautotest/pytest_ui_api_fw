#!/usr/bin/python3
# -*- coding: UTF-8 -*- 
# 设置utf-8  显示中文
"""
@Create Time: 2021/1/14 下午3:25
@Author: guo
@File：handle_ensure_run.py
"""

from common.yml.get_yml_keys import GetYmlKeys

class HandleEnsureRun:

    """处理是否运行,默认值为运行"""

    def get_ensure_run(self,page_api_data:dict,yml_keys:GetYmlKeys,case_api_data:dict=None,req_keys:list=None)->bool:
        """
        获取是否运行,默认值为True. 以case层为准,但是也要考虑page层的值.因为有可能会出现: 在case层没有写该key的情况.  \n
        :param page_api_data: 对应的page层的yml里的 api_data 的数据
        :param yml_keys: yml的keys
        :param case_api_data: 传入的case层 yml数据 对应的 api_data的数据
        :param req_keys: 传入的case层的yml数据 对应的key
        :return: bool
        """
        # 获取各参数的值
        api_data = page_api_data
        yml_keys = yml_keys
        req_params = case_api_data
        req_keys = req_keys

        flag = True
        key = yml_keys.get_run_whether_key()
        key_value = None
        if req_keys != None:
            if key in req_keys:
                key_value = req_params[key]

        # 先判断case层的值,然后再判断page层的值
        if key_value == None:
            # 获取page层的值是否为false
            if api_data[key] == False:
                flag = False
        elif key_value == False:
            flag = False

        return flag

