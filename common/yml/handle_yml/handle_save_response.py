#!/usr/bin/python3
# -*- coding: UTF-8 -*- 
# 设置utf-8  显示中文
"""
@Create Time: 2021/1/14 下午3:47
@Author: guo
@File：handle_save_response.py
"""
import pytest
import sys

sys.path.append("../../")
from common.yml.get_yml_keys import GetYmlKeys
from config.get_conf_data import GetConfData
from common.util.get_file_path import GetFilePath


class HandleSaveResponse:
    """"""
    def __init__(self):
        self.__conf = GetConfData()
        self.__cfile = GetFilePath()

    def get_save_response(self,page_api_data:dict,yml_keys:GetYmlKeys,case_api_data:dict=None,req_keys:list=None)->dict:
        """
        获取是否保存当前的响应结果,默认是不保存的.以及save_type,save_response的处理.  \n
        save_type: 有三种类型,json,sql,all(json+sql).默认值为 json  \n
        response_filepathname: 为保存的json文件,在jsonfile目录下的相对路径. 会校验书写的正确性. \n
        :param page_api_data: 对应的page层的yml里的 api_data 的数据
        :param yml_keys: yml的keys
        :param case_api_data: 传入的case层 yml数据 对应的 api_data的数据
        :param req_keys: 传入的case层的yml数据 对应的key
        :return: dict
        """
        api_data = page_api_data
        yml_keys = yml_keys
        req_params = case_api_data
        req_keys = req_keys

        flag = False
        ensre_key = yml_keys.get_ensure_save_response_key()
        key_value = None
        save_key = yml_keys.get_save_response_key()

        if req_keys != None:
            if ensre_key in req_keys:
                # 获取case层里对应的值
                key_value = req_params[ensre_key]

        # if key_value == None or not isinstance(key_value,bool):
        #     flag = flag
        #
        # elif key_value == True:
        #     flag = True

        if key_value == True:
            flag = True

        # 进行赋值
        api_data[ensre_key] = flag
        # 判断是否要保存响应结果
        if flag == False:
            api_data[save_key] = None

        # 表示需要保存响应结果.
        else:
            """
            第一步: 为防止出现在后面获取 key对应值时,提示keyerror错,所以可以通过for循环,将case层的save_response的值赋值给page层
            """
            # 先检测case层是否存在相应的key,不存在,则将case置为fail
            if save_key not in req_keys:
                pytest.fail(f"由于ensure_save_response的值为: {flag} ,而传入的值里没有 save_response,"
                            f"所以将该case置为fail")

            save_response:dict = req_params[save_key]
            for key in save_response.keys():
                api_data[save_key][key] = save_response[key]

            """
            第二步: 检测save_type的值 
            """
            type_key = yml_keys.get_save_type_key()
            type_value = api_data[save_key][type_key]

            json_str =self.__conf.get_json_str()
            sql_str = self.__conf.get_sql_str()
            all_str = self.__conf.get_all_str()
            type = json_str


            # 校验值
            if type_value == None or not isinstance(type_value,str):
                type = type

            else:
                type_value = str.lower(type_value)

                if type_value in all_str:
                    type = all_str
                elif type_value in sql_str:
                    type = sql_str
                else :
                    type = json_str
            # 进行赋值
            api_data[save_key][type_key] = type

            """
            第三步, 检测response_filepathname的书写的正确性,并进行修正.当值为空 或非str类型时
            直接将case置为fail
            """
            file_key = yml_keys.get_response_filepath_key()
            filepath = api_data[save_key][file_key]
            # 进行判断
            if filepath == None or not isinstance(filepath,str):
                pytest.fail(f"传入的{ensre_key}的值为: True ,但是 {file_key}的值为: {filepath},不是str类型,"
                            f"即传入的 {file_key} 的值不合法,故将case置为fail")

            else:
                # 检测传入的filepath 是否包含了 filepath目录.同时返回该目录的绝对路径.
                filepath = self.__cfile.get_file_abspath_json(filepath)
                # 进行赋值
                api_data[save_key][file_key] = filepath

        # 返回结果
        return api_data



