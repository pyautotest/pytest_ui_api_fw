#!/usr/bin/python3
# -*- coding: UTF-8 -*- 
# 设置utf-8  显示中文
"""
@Create Time: 2021/1/14 下午3:47
@Author: guo
@File：handle_assert.py
"""
from common.yml.get_yml_keys import GetYmlKeys


class HandleAssert:

    """"""

    def get_assert(self,page_api_data:dict,yml_keys:GetYmlKeys,case_api_data:dict=None,req_keys:list=None)->None or list:
        """
        将case层的断言,传入到page层.  \n
        :param page_api_data: 对应的page层的yml里的 api_data 的数据
        :param yml_keys: yml的keys
        :param case_api_data: 传入的case层 yml数据 对应的 api_data的数据
        :param req_keys: 传入的case层的yml数据 对应的key
        :return: None or list(assert的值)
        """
        api_data = page_api_data
        yml_keys = yml_keys
        req_params = case_api_data
        req_keys = req_keys

        assert_key = yml_keys.get_assert_key()
        assert_value = None
        # 判断传入的case请求 是否为None
        if req_keys !=None:
            if assert_key in req_keys:
                assert_value = req_params[assert_key]

        # # 此时无需再向其他的版块一样进行判断.因为在执行 assert断言时,是放在case层进行断言的.
        # api_data[assert_key] = assert_value

        # 返回该值
        return assert_value



