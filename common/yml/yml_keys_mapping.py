#!/usr/bin/python3
# -*- coding: UTF-8 -*- 
# 设置utf-8  显示中文
"""
@Author: guozg
@File：yml_keys_mapping.py
"""

class YmlKeys:
    """case、page 对应的yml文件所涉及到的key"""

    def __init__(self):

        """--------------- yml 文件中的key ----------------"""
        # yml文件中 type 对应的key
        self.type_key = "type"
        # yml文件中 api_data 对应的key
        self.api_data_key = "api_data"
        # yml文件中 run_whether 对应的key
        self.run_whether_key = "run_whether"
        # yml文件中 host 对应的key
        self.host_key = "host"
        # yml文件中 path 对应的key
        self.path_key = "path"
        # data_type 对应的key
        self.data_type_key = "data_type"
        # request_data 对应的key
        self.request_data_key = "request_data"
        # method 对应的key
        self.method_key = "method"
        # url 对应的key
        self.url_key = "url"
        # dependence_login 对应的key
        self.dependence_login_key = "dependence_login"
        # login_scope 对应的key
        self.login_scope_key = "login_scope"
        # login_type 对应的key
        self.login_type_key = "login_type"
        # token_filepath 对应的key
        self.token_filepath_key = "token_filepath"
        # login_data 对应的key
        self.login_data_key = "login_data"
        # loginId 对应的key
        self.loginId_key = "loginId"
        # username 对应的key
        self.username_key = "username"
        # password 对应的key
        self.password_key = "password"
        # yml文件中 dependence_case 对应的key
        self.dependence_case_key = "dependence_case"
        # yml文件中 dependence_case_data 对应的key
        self.dependence_case_data_key = "dependence_case_data"
        # yml文件中 dependence_type 对应的key
        self.dependence_type_key = "dependence_type"
        # yml文件中 jsonfilepath 对应的key
        self.jsonfilepath_key = "jsonfilepath"
        # yml文件中 sqlstatement 应对的key
        self.sqlstatement_key = "sqlstatement"
        # case id 对应的key
        self.case_id_key = "case_id"
        # dependence_key 对应的key
        self.dependence_key = "dependence_key"
        # replace_key 对应的key
        self.replace_key = "replace_key"
        # assert 对应的key
        self.assert_key = "assert"

        #-------------------后增加的一些key,主要用于数据的校验以及数据的保存等------------------
        # ensure_save_response 对应的key
        self.ensure_save_response_key = "ensure_save_response"
        # save_response 对应的key
        self.save_response_key = "save_response"
        # save_type 对应的key
        self.save_type_key = "save_type"
        # response_filepath 对应的key
        self.response_filepath_key = "response_filepath"
        # ensure_check 对应的key
        self.ensure_check_key = "ensure_check"
        # check_data 对应的key
        self.check_data_key = "check_data"
        # ensure_return 对应的key
        self.ensure_return_key = "ensure_return"
        # save_jsonfile_path 对应的key
        self.save_jsonfile_path_key = "save_jsonfile_path"
        # save_keys 对应的key
        self.save_keys_key = "save_keys"
        # query_keys 对应的key
        self.query_keys_key = "query_keys"
        # amount 对应的key
        self.amount_key = "amount"
        # expect_data 对应的key
        self.expect_data_key = "expect_data"
        # actual_data 对应的key
        self.actual_data_key = "actual_data"
        # ensure_checktime 对应的key
        self.ensure_checktime_key = "ensure_checktime"
        # time_delta 对应的key
        self.time_delta_key = "time_delta"
        # time_type 对应的key
        self.time_type_key = "time_type"
        # timedelta_value 对应的key
        self.timedelta_value_key = "timedelta_value"
        # create_time 对应的key
        self.create_time_key = "create_time"




