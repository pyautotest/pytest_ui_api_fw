#!/usr/bin/python3
# -*- coding: UTF-8 -*- 
# 设置utf-8  显示中文
"""
@Author: guo
@File：generate_device_key.py
@Description: 
1.
2.
"""

import random


class GenerateDeviceKey:
    '''
    主要是针对 移动端生成设备号，要保证设备号是唯一的。  \n
    移动端在登录时，需要值进入设备号，要保证设备号是唯一的。  \n
    '''
    global letters_list
    letters_list = ['z', 'y', 'x', 'w', 'v', 'u', 't', 's', 'r', 'q', 'p', 'o', 'n', 'm', 'l', 'k', 'j', 'i', 'h', 'g',
                    'f', 'e', 'd', 'c', 'b', 'a']

    def generate_key_no_account(self) -> str:
        '''
        生成设备号，不包括账号，格式为【5位的数字+5位的字母】
        :return: 格式为【5位的数字+5位的字母】的字符串
        '''
        letters_key = self.__generate_str()
        number_key = self.__generate_nubmer()
        key = number_key + letters_key
        return key

    def generate_key_by_account(self, account) -> str:
        '''
        生成设备号，有账号，格式为【5位数字+5位字母+传入的账号+5位字母】
        :param account: 账号
        :return: 格式为【5位数字+5位字母+传入的账号+5位字母】的字符串
        '''
        letters_key1 = self.__generate_str()
        letters_key2 = self.__generate_str()
        number_key = self.__generate_nubmer()
        key = number_key + letters_key1 + str(account) + letters_key2
        return key

    def __generate_nubmer(self) -> str:
        '''
        生成10000到99999之间的随机数，并进行强制转换为str类型
        :return: 5位随机数 str
        '''
        return str(random.randint(10000, 99999))

    def __generate_str(self):
        '''
        生成5位长度的字母字符串
        :return: 5个字母
        '''
        return "".join(random.sample(letters_list, 5))

