# pytest_ui_api_fw

#### Description
1：该框架是基于pyest、requests、python3 写的一个 api接口自动化测试框架，后期会将 接入ui自动化。该接口自动化框架的思想与 ui 自动化测试框架的思想一致，使用po模式，分为page业务层、case数据层，以及base层。
2：该框架的数据源为yml文件，上手有一定的难度。该框架在后期维护的时候，非常方便，只需要简单的维护yml文件即可。
3：最后的报告是使用allure
4：本框架已在本公司深度使用，通过jenkins持续集成，每天会定时执行，向测试小组发送执行结果，也会不定期的巡检公司生产环境的功能等。
5：该框架后期还会有多个分支版本，这些多分支版本均是来优化和提升执行效率的。
6：由于本人能力有限，该框架里会存在着bug，也是在所难免的。

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
