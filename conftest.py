#!/usr/bin/python
# -*- coding: UTF-8 -*- 
# 设置utf-8  显示中文
"""
@Create Time: 20年 11-14  21:27
@Author: guo 
@File：conftest.py
"""
import json
from string import Template

import pytest
import requests
import sys

from common.util.get_file_path import GetFilePath
from common.util.handle_jsonfile import HandleJsonFile
from common.yml.get_yml_keys import GetYmlKeys

sys.path.append("../")
from common.cookie.get_web_cookie import GetWebCookie
from common.cookie.get_app_cookie_header import GetAppCookieHeader
from common.util.generate_device_key import GenerateDeviceKey
from config.get_conf_data import GetConfData

get_conf_data = GetConfData()
ymlkeys = GetYmlKeys()
fpath = GetFilePath()
hjson = HandleJsonFile()
web_login_data: dict = get_conf_data.get_web_login_request_data()
app_login_data: dict = get_conf_data.get_app_login_request_data()

logindata_key = ymlkeys.get_login_data_key()
res_filepath_key = ymlkeys.get_response_filepath_key()
token_key = ymlkeys.get_token_filepath_key()

@pytest.fixture(scope="class")
def web_login(request):
    login_data = request.param[logindata_key]
    res_filepath = request.param[res_filepath_key]
    token_path = request.param[token_key]

    global web_login_data
    web_login_data = __replace(web_login_data,login_data)
    res = requests.request(**web_login_data)
    get_cookies = GetWebCookie(res)

    # 当response_filepath 不为None时,则保存当前的登录响应结果
    if res_filepath != None:
        res_filepath = fpath.get_file_abspath_json(res_filepath)
        hjson.write_dict_to_json_file(res_filepath, res.json())
    # 当token_filepath 不为None时,则保存当前的cookie
    if token_path != None:
        token_path = fpath.get_file_abspath_json(token_path)
        token = get_cookies.get_cookie()
        hjson.write_dict_to_json_file(token_path, token)

    # return get_cookies


@pytest.fixture(scope="class")
def app_login(request):
    login_data = request.param[logindata_key]
    res_filepath = request.param[res_filepath_key]
    token_path = request.param[token_key]

    loginid = login_data['loginId']
    global app_login_data
    app_login_data = __replace(app_login_data,login_data)
    app_login_data['device'] = GenerateDeviceKey().generate_key_by_account(loginid)
    res = requests.request(**app_login_data)
    get_header = GetAppCookieHeader(res)

    # 当response_filepath 不为None时,则保存当前的登录响应结果
    if res_filepath != None:
        res_filepath = fpath.get_file_abspath_json(res_filepath)
        hjson.write_dict_to_json_file(res_filepath, res.json())
    # 当token_filepath 不为None时,则保存当前的cookie
    if token_path != None:
        token_path = fpath.get_file_abspath_json(token_path)
        token = get_header.get_header()
        hjson.write_dict_to_json_file(token_path, token)

    # return get_header


@pytest.fixture(scope="class")
def get_web_login_response(request):
    login_data = request.param[logindata_key]
    res_filepath = request.param[res_filepath_key]
    token_path = request.param[token_key]

    global web_login_data
    web_login_data=__replace(web_login_data,login_data)
    res = requests.request(**web_login_data)

    # 当response_filepath 不为None时,则保存当前的登录响应结果
    if res_filepath != None:
        res_filepath = fpath.get_file_abspath_json(res_filepath)
        hjson.write_dict_to_json_file(res_filepath, res.json())
    # 当token_filepath 不为None时,则保存当前的cookie
    if token_path != None:
        token_path = fpath.get_file_abspath_json(token_path)
        get_cookies = GetWebCookie(res)
        token = get_cookies.get_cookie()
        hjson.write_dict_to_json_file(token_path, token)

    # return res


@pytest.fixture(scope="class")
def get_app_login_response(request):
    login_data = request.param[logindata_key]
    res_filepath = request.param[res_filepath_key]
    token_path = request.param[token_key]

    loginid = login_data['loginId']
    global app_login_data
    app_login_data = __replace(app_login_data,login_data)
    app_login_data['data']['device'] = GenerateDeviceKey().generate_key_by_account(loginid)
    res = requests.request(**app_login_data)

    # 当response_filepath 不为None时,则保存当前的登录响应结果
    if res_filepath != None:
        res_filepath = fpath.get_file_abspath_json(res_filepath)
        hjson.write_dict_to_json_file(res_filepath, res.json())
    # 当token_filepath 不为None时,则保存当前的cookie
    if token_path != None:
        token_path = fpath.get_file_abspath_json(token_path)
        get_header = GetAppCookieHeader(res)
        token = get_header.get_header()
        hjson.write_dict_to_json_file(token_path, token)

    # return res

def __replace(templace,login_data:dict):
    tem = Template(json.dumps(templace))
    res = json.loads(tem.substitute(**login_data))
    return res
